# Wifimgr

Wifimanager - A easy to use command line utility for managing wifi networks

Latest release: 0.1.4-stable

# Installation & usage

1. Use ./src/install.sh
2. Use ./bin/Wifimgr
3. Copy ./bin/wifimgr to /usr/bin/

# Commands

 * list - list the wifi networks in a wireless interface
 * connect - connect to a wifi network in a given wireless interface
 * unblock - unblock wifi if blocked by rfkill
 * clear - clear the console
 * exit - exit out of wifimgr
 * enable <interface> - enable the wireless interface to use if already disabled
 * disable - disable a given wirless interface

